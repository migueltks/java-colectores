package Pruebas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.tandem.ext.guardian.ProcessHandle;

public class Principal {
	
	static FileWriter w;
	static BufferedWriter bw;
	static PrintWriter wr;
	static File f;
	
	public static void escribir(String Tx){
		File f;
		f = new File("Prueba");
	
		//ESCRITURA
		try{
			FileWriter w = new FileWriter(f,true);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
						 			
			//wr.write("Esta es una linea de codigo");//escribimos en el archivo 
			wr.append(Tx + "\n"); //concatenamos en el archivo sin borrar lo existente
			              //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedar� guardado con informaci�n escrita
			              //de no hacerlo no se escribir� nada en el archivo
			wr.close();
			bw.close();
		}catch(IOException e){};       
	}
	
	public static void main(String[] args) throws Exception {
		try
        {	
			//System.out.println(Cipher.getMaxAllowedKeyLength("AES"));
			 
			/*
			 final String strToEncrypt = "My text to encrypt";
			 final String strPssword = "encryptor key";
			 Principal.setKey(strPssword);
			 Principal.encrypt(strToEncrypt.trim());
			 System.out.println("String to Encrypt: " + strToEncrypt);
			 System.out.println("Encrypted: " + Principal.getEncryptedString());
			 		 
			 final String strToDecrypt = Principal.getEncryptedString();
			 Principal.decrypt(strToDecrypt.trim());
			 System.out.println("String To Decrypt : " + strToDecrypt);
			 System.out.println("Decrypted : " + Principal.getDecryptedString());
			*/
			 
			 
			
			/*short VLintID;
			String VLstrProceso;
			ProcessHandle VLobjDatos = ProcessHandle.getMine();
			String VLstrPwd = "Pwd*KS";
			//String VLstrDecrypt;
			
			VLintID = VLobjDatos.getCPU();
			VLstrProceso = VLobjDatos.getProcessName();
            
			escribir("ID: " + VLobjDatos.getCPU());
            Principal.setKey(VLstrPwd + VLintID + VLstrProceso);
			Principal.encrypt(Short.toString(VLintID).trim());
			escribir("Cadena a cambiar: " + Short.toString(VLintID).trim());
			escribir("Cambio: " + Principal.getEncryptedString());
						
            escribir("Proceso: " + VLobjDatos.getProcessName());
			Principal.setKey(VLstrPwd + VLintID + VLstrProceso);
			Principal.encrypt(Short.toString(VLintID).trim());
			escribir("Cadena a cambiar: " + Short.toString(VLintID).trim());
			escribir("Cambio: " + Principal.getEncryptedString());
			*/
			ProcessHandle VLobjDatos = ProcessHandle.getMine();
			System.out.println("id: " + VLobjDatos.getCPU());
			System.out.println("name: " + VLobjDatos.getProcessName());
        }
        catch (Exception ex)
        {

        }	
		
    }
	
		
		
		
	 private static SecretKeySpec secretKey ;
	 private static byte[] key ;
	     private static String decryptedString;
	     private static String encryptedString;
	 public static void setKey(String myKey){
	     
	     MessageDigest sha = null;
	         try {
	             key = myKey.getBytes("UTF-8");
	             System.out.println(key.length);
	             sha = MessageDigest.getInstance("SHA-1");
	             key = sha.digest(key);
	          key = Arrays.copyOf(key, 16); // use only first 128 bit
	          System.out.println(key.length);
	          System.out.println(new String(key,"UTF-8"));
	          secretKey = new SecretKeySpec(key, "AES");
	         
	         
	         } catch (NoSuchAlgorithmException e) {
	             // TODO Auto-generated catch block
	             e.printStackTrace();
	         } catch (UnsupportedEncodingException e) {
	             // TODO Auto-generated catch block
	             e.printStackTrace();
	         }	     
	 }
	 
	 public static String getDecryptedString() {
		 return decryptedString;
	 }
	 
	 public static void setDecryptedString(String decryptedString) {
		 Principal.decryptedString = decryptedString;
	 }
	 
	 public static String getEncryptedString() {
		 return encryptedString;
	 }
	 
	 public static void setEncryptedString(String encryptedString) {
	   	 Principal.encryptedString = encryptedString;
	 }
	 
	 public static String encrypt(String strToEncrypt)
	 {
		 try
	 	{
		 	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			setEncryptedString(Base64.encode(cipher.doFinal(strToEncrypt.getBytes("UTF-8"))));
	 	}
		catch (Exception e)
		{
			System.out.println("Error while encrypting: "+e.toString());
		}
		return null;
	 }
	 
	 public static String decrypt(String strToDecrypt)
	 {
		 try
		 {
			 Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			 cipher.init(Cipher.DECRYPT_MODE, secretKey);
			 setDecryptedString(new String(cipher.doFinal(Base64.decode(strToDecrypt))));
		 }
		 catch (Exception e)
		 {
			 System.out.println("Error while decrypting: "+e.toString());
		 }
		 return null;
	 }
}
